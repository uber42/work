/*
** 1. ��������� ��������� ������ �� ������������ ������ ����� � ������������� ������ ������� �� ����
** 2. ����� �������� ������� ���� data.txt � ���������� � ����� ��������� ������
** 3. ����� ������ ����� �������� ���������� 10 ������� ���������, ������� ��������� ��������� ��������� ������ � ����� (������������� ����� mutex)
** 4. ��� ���������� �������
**
** ��� ����� ������ exit ������� �� �����.
*/


#include <stdio.h>
#include <stdbool.h>
#include <Windows.h>
#include <string.h>

#define BUFF_SIZE 256					// ������ ������

#define NUM_READERS 10					// ���������� ���������

HANDLE hMutex;
HANDLE hReadEvent;						// ������� ������
HANDLE hWriteEvent;						// ������� ������

DWORD WINAPI write(CONST LPVOID arg);	// ������� ������
DWORD WINAPI read(CONST LPVOID arg);	// ������� ������

const char filename[] = "data.txt";		// �������� �����
char buff[BUFF_SIZE];					// �����
long int pos;							// ������� ��� ������ � �����
int readCount;							// ������� ������ �������

int main() {
	HANDLE writer,                                                           // ����� ��������
		   readers[NUM_READERS];											 // ������ ��������

	// �������� �������� �������������
	hMutex = CreateMutex(NULL, FALSE, NULL);
	hReadEvent = CreateEvent(NULL, TRUE, FALSE, TEXT("ReadEvent"));
	hWriteEvent = CreateEvent(NULL, TRUE, FALSE, TEXT("WriteEvent"));

	pos = 0;
	readCount = 0;

	// ��������� ������ ��������� � ��������� �������� �������������
	if (hMutex == NULL) {
		printf("Error CreateMutex() : %d\n", GetLastError());
		return 1;
	}
	if (hReadEvent == NULL)
	{
		printf("Error CreateEvent() : %d\n", GetLastError());
		return 1;
	}
	if (hWriteEvent == NULL)
	{
		printf("Error CreateEvent() : %d\n", GetLastError());
		return 1;
	}

	// �������� ������� ���������
	for (int i = 0; i < NUM_READERS; i++) {
		readers[i] = CreateThread(NULL, 0, &read, NULL, 0, NULL);

		// ��������� ������ ��� �������� ������
		if (readers[i] == NULL) {
			printf("Error CreateThread() : %d\n", GetLastError());

			// �������� ��� ��������� �������
			for (int j = 0; j < i; j++) {
				CloseHandle(readers[j]);
			}

			return 1;
		}
	}

	// �������� ������ ��������
	writer = CreateThread(NULL, 0, &write, NULL, 0, NULL);

	// ��������� ������ ��� �������� ������
	if (writer == NULL) {
		printf("Error CreateThread() : %d\n", GetLastError());

		// �������� ������� ���������
		for (int j = 0; j < NUM_READERS; j++) {
			CloseHandle(readers[j]);
		}

		return 1;
	}

	/* 
	** �������� ���� ���������
	** ��������� ������ � ������������� ������ ��������, ����� �������� ���������� ����������� ���������
	** ���� �������� exit ��������� ����
	*/
	while (TRUE) {
		gets(buff);
		if (strcmp(buff, "exit") == 0) {
			break;
		}

		// ������ ������ ��������
		PulseEvent(hWriteEvent);
	}
	
	// �������� ���� ������������
	for (int j = 0; j < NUM_READERS; j++) {
		CloseHandle(readers[j]);
	}
	CloseHandle(writer);

	CloseHandle(hMutex);
	CloseHandle(hReadEvent);
	CloseHandle(hWriteEvent);

	return 0;
}

DWORD WINAPI write(CONST LPVOID arg)
{
	// �������� ������� �����
	FILE *fstream;
	// ��������� ������ ��� �������� �����
	errno_t err = fopen_s(&fstream, filename, "w");
	if (err != 0) {
		printf("Error fopen_s() : %d\n", err);
	}
	// �������� ������
	fclose(fstream);

	DWORD threadId = GetThreadId(GetCurrentThread());
	printf("Thread %d writes\n", threadId);

	// ���� ��������
	while (TRUE) {
		// �������� ����� ������
		WaitForSingleObject(hWriteEvent, INFINITE);

		// �������� ����� ��� ������ � �����
		FILE *fstream;
		errno_t err = fopen_s(&fstream, filename, "a");
		if (err != 0) {
			printf("Error fopen_s() : %d\n", err);
			return 0;
		}

		fprintf(fstream, "%s\n", buff);
		printf("Thread %d wrote into file %s\n", threadId, buff);

		// �������� ������
		fclose(fstream);

		// ������� ���������
		PulseEvent(hReadEvent);
	}

	
	return 1;
}

DWORD WINAPI read(CONST LPVOID arg)
{
	DWORD threadId = GetThreadId(GetCurrentThread());
	printf("Thread %d reads\n", threadId);

	// ���� ��������
	while (TRUE) {
		// �������� �������� �� ��������
		WaitForSingleObject(hReadEvent, INFINITE);

		// ������ ������ �� ������� ������ �������
		DWORD dwWaitResult = WaitForSingleObject(hMutex, INFINITE);

		

		switch (dwWaitResult)
		{
		// ����� ������� ����� ������������� �� ������
		case WAIT_OBJECT_0:

			FILE *fstream;
			errno_t err;

			// �������� ����� ��� ������
			err = fopen_s(&fstream, filename, "r");
			if (err != 0) {
				printf("Error fopen_s() : %d\n", err);
			}

			// ���������� �������� ���������� ������� ������� ��������� ����
			readCount++;

			// ������������ ������� ���������� �� ������ ����� ������ � �����
			fseek(fstream, pos, SEEK_SET);

			// ���������� ������
			char buff[BUFF_SIZE];
			if (fgets(buff, BUFF_SIZE, fstream) == NULL) {
				printf("Error fgets() in thread : %d\n", threadId);
			}
			else {
				printf("Thread %d read %s", threadId, buff);
			}

			// ��� ��������� ����� ����� �������� 
			if (readCount == NUM_READERS) {
				readCount = 0;

				// ����� �������
				pos = ftell(fstream);
			}

			// �������� ��������� ������
			fclose(fstream);

			break;
		// ����� ������� �������� �� ������ ������� ���� ����� � ����������
		case WAIT_ABANDONED:
			printf("Error WaitForSingleObject() in thread : %d\n", threadId);
			return 0;
			break;
		}

		// ������������ ������ �� �������� �������
		ReleaseMutex(hMutex);
	}

	return 0;
}